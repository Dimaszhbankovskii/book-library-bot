from aiogram.types import ReplyKeyboardMarkup, KeyboardButton


def get_main_keyboards() -> ReplyKeyboardMarkup:
    buttons = [
        [
            KeyboardButton(text="👋 Поздороваться")
        ],
        [
            KeyboardButton(text="Библиотека")
        ]
    ]
    keyboard = ReplyKeyboardMarkup(
        keyboard=buttons,
        resize_keyboard=True,
        input_field_placeholder="Выберите действие"
    )
    return keyboard

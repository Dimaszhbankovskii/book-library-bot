import logging
from aiogram import Router, F
from aiogram.filters import Command
from aiogram.types import Message

from keyboards.main_keyboards import get_main_keyboards


router = Router()


@router.message(Command('start'))
async def cmd_start(message: Message):
    logging.info('Bot starting to work')
    await message.answer(text=f'Привет, {message.from_user.first_name}! Я бот для взаимодействия с библиотекой книг.',
                         reply_markup=get_main_keyboards())


@router.message(F.text == '👋 Поздороваться')
async def mess_hello(message: Message):
    await message.answer(text="Привет! Спасибо, что пользуешься мной!")

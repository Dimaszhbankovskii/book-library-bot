import uuid
import datetime
from sqlalchemy.schema import Column
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql.sqltypes import VARCHAR, Integer, ARRAY, TIMESTAMP

from models.db import Base


class Book(Base):
    __tablename__ = 'book'

    id = Column(UUID(as_uuid=True), nullable=False, unique=True, primary_key=True, default=uuid.uuid4)
    name = Column(VARCHAR(200), nullable=False)
    amount = Column(Integer, nullable=False)
    locations = Column(ARRAY(VARCHAR(200)))
    created_at = Column(TIMESTAMP(timezone=True), nullable=False, default=datetime.datetime.now)
    updated_at = Column(TIMESTAMP(timezone=True), nullable=False, default=datetime.datetime.now,
                        onupdate=datetime.datetime.now)

    def __repr__(self):
        return "<{0.__class__.__name__}(id={0.id!r})>".format(self)

from aiogram.client.bot import Bot
from aiogram.dispatcher.dispatcher import Dispatcher

from settings import creds


class BotOperator:

    def __init__(self,
                 token: str) -> None:
        self.token = token
        self.bot = Bot(token=self.token)
        self.dp = Dispatcher()

    def include_routers(self, routers: list) -> None:
        for router in routers:
            self.dp.include_router(router)


bot_operator: BotOperator = BotOperator(token=creds.bot_token)

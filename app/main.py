import asyncio

from services.bot import bot_operator
from handlers import main_commands


async def main() -> None:
    bot_operator.include_routers(
        [
            main_commands.router,
        ]
    )
    await bot_operator.bot.delete_webhook(drop_pending_updates=True)
    await bot_operator.dp.start_polling(bot_operator.bot)


if __name__ == '__main__':
    asyncio.run(main())

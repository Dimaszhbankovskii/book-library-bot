from pydantic import Field
from pydantic_settings import BaseSettings


class Creds(BaseSettings):
    pg_dsn: str = Field("postgresql+asyncpg://bot:bot@localhost:32768/postgres",
                        env="PG_DSN",
                        description="url для postgres")
    bot_token: str = Field("6396497098:AAG77nISt8lhp7oYUbEmPk3EHzAo8mdXQWo",
                           env="BOT_TOKEN",
                           description="токен для телеграмм бота")


creds: Creds = Creds()
